import React from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                "00AK": {
                    "icao": "00AK",
                    "iata": "",
                    "name": "Lowell Field",
                    "city": "Anchor Point",
                    "state": "Alaska",
                    "country": "US",
                    "elevation": 450,
                    "lat": 59.94919968,
                    "lon": -151.695999146,
                    "tz": "America\/Anchorage"
                },
                "00AL": {
                    "icao": "00AL",
                    "iata": "",
                    "name": "Epps Airpark",
                    "city": "Harvest",
                    "state": "Alabama",
                    "country": "US",
                    "elevation": 820,
                    "lat": 34.8647994995,
                    "lon": -86.7703018188,
                    "tz": "America\/Chicago"
                },
                "00AZ": {
                    "icao": "00AZ",
                    "iata": "",
                    "name": "Cordes Airport",
                    "city": "Cordes",
                    "state": "Arizona",
                    "country": "US",
                    "elevation": 3810,
                    "lat": 34.3055992126,
                    "lon": -112.1650009155,
                    "tz": "America\/Phoenix"
                },
                "00CA": {
                    "icao": "00CA",
                    "iata": "",
                    "name": "Goldstone \/Gts\/ Airport",
                    "city": "Barstow",
                    "state": "California",
                    "country": "US",
                    "elevation": 3038,
                    "lat": 35.3504981995,
                    "lon": -116.888000488,
                    "tz": "America\/Los_Angeles"
                },
                "00CO": {
                    "icao": "00CO",
                    "iata": "",
                    "name": "Cass Field",
                    "city": "Briggsdale",
                    "state": "Colorado",
                    "country": "US",
                    "elevation": 4830,
                    "lat": 40.6222000122,
                    "lon": -104.34400177,
                    "tz": "America\/Denver"
                },
                "00FA": {
                    "icao": "00FA",
                    "iata": "",
                    "name": "Grass Patch Airport",
                    "city": "Bushnell",
                    "state": "Florida",
                    "country": "US",
                    "elevation": 53,
                    "lat": 28.6455001831,
                    "lon": -82.21900177,
                    "tz": "America\/New_York"
                },
                "00FL": {
                    "icao": "00FL",
                    "iata": "",
                    "name": "River Oak Airport",
                    "city": "Okeechobee",
                    "state": "Florida",
                    "country": "US",
                    "elevation": 35,
                    "lat": 27.2308998108,
                    "lon": -80.9692001343,
                    "tz": "America\/New_York"
                },
            }
        }
    }
    componentDidMount() {
        // console.log("DATA is", Object.values(this.state.data))
        this.getData()
    }

    getData = async () => {
        try {
            let response = await fetch(
                'https://raw.githubusercontent.com/mwgg/Airports/master/airports.json'
            );
            let json = await response.json();
            this.setState({ data: json });
        } catch (error) {
            console.error(error);
        }
    }
    renderItem = ({ item, index }) => {
        return <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Details", { "data": item })}
            style={{ margin: 5, borderWidth: 1, padding: 5 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>
                    {item.name}
                </Text>
                <Text>
                    {item.icao}
                </Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>
                    {item.city}{", " + item.state}
                </Text>
                <Text>
                    {item.name}
                </Text>
            </View>
        </TouchableOpacity>
    };

    render() {
        return (
            <View>
                <FlatList
                    data={Object.values(this.state.data)}
                    renderItem={this.renderItem}
                // keyExtractor={(item) => item}
                />
            </View>
        )
    }
}