import React from 'react'
import { View, Text, Platform, Dimensions } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / 10;

const LATITUDE_DELTA = 0.0522;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.route.params.data
        }
    }

    render() {
        return (
            <>
                <MapView
                    style={{ flex: 0.98 }}
                    provider={PROVIDER_GOOGLE}
                    zoomEnabled={true}
                    initialRegion={{
                        latitude: this.state.data.lat,
                        longitude: this.state.data.lon,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }}
                    showsUserLocation
                >
                    <Marker.Animated
                        coordinate={{ latitude: this.state.data.lat, longitude: this.state.data.lon, }}
                    />
                </MapView>
                <View
                    style={{ margin: 5, padding: 5 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>
                            {this.state.data.name}
                        </Text>
                        <Text>
                            {this.state.data.icao}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>
                            {this.state.data.city}{", " + this.state.data.state}{", " + this.state.data.country}
                        </Text>
                        <Text>
                            {this.state.data.name}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>
                            {this.state.data.tz}
                        </Text>
                    </View>
                </View>
            </>

        )
    }
}